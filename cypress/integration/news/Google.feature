Feature: The Google

  I want to open Google page

  @VAS-6 @OPEN
  Scenario: Opening a Google network page
    Given I open Google page
    Then I see "Google" in the title

  @VAS-6 @OPEN
  Scenario: Different kind of opening
    Given I kinda open Google page
    Then I am very happy
